﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Monitoring.Shared.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Monitoring.Shared;
using Monitoring.Shared.Dto;
using Newtonsoft.Json;
using Monitoring.Server.Data.Core;
using Monitoring.Shared.AuthorizationDefinitions;

namespace Monitoring.Storage
{
    public class DatabaseInitializer : IDatabaseInitializer
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole<Guid>> _roleManager;
        private readonly ILogger _logger;

        public DatabaseInitializer(
            ApplicationDbContext context,
            ILogger<DatabaseInitializer> logger,
            UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole<Guid>> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
            _logger = logger;
        }

        public virtual async Task SeedAsync()
        {
            //Apply EF Core migration scripts
            await MigrateAsync();

            await SetHyperTable();

            //Seed users and roles
            await SeedASPIdentityCoreAsync();

            await SeedUserProfileAsync();

            // Seed test data
            await SeedIoTDataAsync();
        }

        private async Task SetHyperTable()
        {
            var sql = $"SELECT create_hypertable('public.\"{nameof(_context.SensorData)}\"', 'Time', if_not_exists => true);";
            await _context.Database.ExecuteSqlRawAsync(sql);
        }

        private async Task MigrateAsync()
        {
            await _context.Database.MigrateAsync().ConfigureAwait(false);
        }

        private async Task SeedIoTDataAsync()
        {
            await _context.Database.ExecuteSqlRawAsync($"TRUNCATE TABLE public.\"{nameof(_context.Locations)}\" CASCADE;");
            await _context.Database.ExecuteSqlRawAsync($"TRUNCATE TABLE public.\"{nameof(_context.SensorTypes)}\" CASCADE;");
            await _context.Database.ExecuteSqlRawAsync($"TRUNCATE TABLE public.\"{nameof(_context.Sensors)}\" CASCADE;");
            await _context.Database.ExecuteSqlRawAsync($"TRUNCATE TABLE public.\"{nameof(_context.Dashboards)}\" CASCADE;");
            await _context.Database.ExecuteSqlRawAsync($"TRUNCATE TABLE public.\"{nameof(_context.Units)}\" CASCADE;");
            await _context.Database.ExecuteSqlRawAsync($"TRUNCATE TABLE public.\"{nameof(_context.Thresholds)}\" CASCADE;");
            await _context.Database.ExecuteSqlRawAsync($"TRUNCATE TABLE public.\"{nameof(_context.SensorData)}\" CASCADE;");

            #region Locations

            var brno = new Location
            {
                Id = 1,
                DisplayName = "Servis B1",
                Name = "ServisBrno1",
                Address = "Nekde 42",
                ZipCode = "60200",
                City = "Brno",
                PhoneContact = "+420 123 456 789",
                Note = "Arbitrary note for literally anything"
            };

            var praha = new Location
            {
                Id = 2,
                DisplayName = "Servis P1",
                Name = "ServisPraha1",
                Address = "Nekde 42",
                ZipCode = "60200",
                City = "Praha",
                PhoneContact = "+420 123 456 799",
                Note = "Arbitrary note for literally anything"
            };

            var ostrava = new Location
            {
                Id = 3,
                DisplayName = "Servis O1",
                Name = "ServisOstrava1",
                Address = "Nekde 42",
                ZipCode = "60200",
                City = "Ostrava",
                PhoneContact = "+420 123 456 799",
                Note = "Arbitrary note for literally anything"
            };

            if (!await _context.Locations.AnyAsync())
            {
                await _context.Locations.AddRangeAsync(brno, praha, ostrava);
            }

            #endregion Locations

            #region sensorTypes

            var electricity = new SensorType { Id = 1, Name = "Electricity" };
            var pressure = new SensorType { Id = 2, Name = "Pressure" };
            var temperature = new SensorType { Id = 3, Name = "Temperature" };
            var fluidLevel = new SensorType { Id = 4, Name = "Fluid level" };
            var fluidflow = new SensorType { Id = 5, Name = "Fluid flow" };

            if (!await _context.SensorTypes.AnyAsync())
            {
                await _context.SensorTypes.AddRangeAsync(electricity, pressure, temperature, fluidflow, fluidLevel);
            }

            #endregion

            #region Units

            var unitWatt = new Unit { Id = 1, Name = "Watt", Symbol = "W" };
            var unitVolume = new Unit { Id = 2, Name = "Litre", Symbol = "L" };
            var unitTemperature = new Unit { Id = 3, Name = "Celsius", Symbol = "°C" };
            var unitPressure = new Unit { Id = 4, Name = "Pascal", Symbol = "Pa" };

            if (!await _context.Units.AnyAsync())
            {
                await _context.Units.AddRangeAsync(unitWatt, unitVolume, unitTemperature, unitPressure);
            }

            #endregion

            #region SensorStuff

            var sensorModel1 = new SensorModel
            {
                Id = 1,
                Manufacturer = "Manufacturer 1",
                Model = "Model 1",
                PowerSupplyVoltage = "5V",
                Unit = unitWatt,
                SensorType = electricity,
                ImagePath = "/images/sensorModels/sm1.jpg"
            };
            var sensorModel2 = new SensorModel
            {
                Id = 2,
                Manufacturer = "Manufacturer 1",
                Model = "Model 2",
                PowerSupplyVoltage = "12V",
                Unit = unitWatt,
                SensorType = electricity,
                ImagePath = "/images/sensorModels/sm2.jpg"
            };

            var sensorModel3Oil = new SensorModel
            {
                Id = 3,
                Manufacturer = "Manufacturer 2",
                Model = "Model O2",
                PowerSupplyVoltage = "12V",
                Unit = unitVolume,
                SensorType = fluidLevel,
                ImagePath = "/images/sensorModels/sm3.jpg"
            };

            var sensorModel4Temp = new SensorModel
            {
                Id = 4,
                Manufacturer = "Manufacturer 2",
                Model = "Model T1",
                PowerSupplyVoltage = "12V",
                Unit = unitTemperature,
                SensorType = temperature,
                ImagePath = "/images/sensorModels/sm4.jpg"
            };

            var sensorModel5Water = new SensorModel
            {
                Id = 5,
                Manufacturer = "Manufacturer 3",
                Model = "Model W1",
                PowerSupplyVoltage = "5V",
                Unit = unitVolume,
                SensorType = fluidLevel,
                ImagePath = "/images/sensorModels/sm5.jpg"
            };

            var sensorModel6WaterTemp = new SensorModel
            {
                Id = 6,
                Manufacturer = "Manufacturer 1",
                Model = "Model WT1",
                PowerSupplyVoltage = "5V",
                Unit = unitTemperature,
                SensorType = temperature,
                ImagePath = "/images/sensorModels/sm6.jpg"
            };

            if (!await _context.SensorModels.AnyAsync())
            {
                var sensorModels = new SensorModel[]
                {
                    sensorModel1, sensorModel2, sensorModel3Oil, sensorModel4Temp, sensorModel5Water, sensorModel6WaterTemp
                };
                await _context.SensorModels.AddRangeAsync(sensorModels);
            }

            var sensorB1E = new Sensor
            {
                Id = 1,
                Name = "BSensor1",
                DisplayName = "Elektro-A1",
                InstallationDate = DateTime.UtcNow.AddDays(-50),
                SensorModel = sensorModel1,
                Location = brno,
                ImagePath = "images/sensors/s1.jpg"
            };

            var sensorB2Oil = new Sensor
            {
                Id = 2,
                Name = "BSensor3",
                DisplayName = "Oil-A1",
                InstallationDate = DateTime.UtcNow.AddDays(-50),
                SensorModel = sensorModel3Oil,
                Location = brno,
                ImagePath = "images/sensors/s2.jpg"
            };

            var sensorB3E = new Sensor
            {
                Id = 3,
                Name = "BSensor3",
                DisplayName = "Elektro1-A1",
                InstallationDate = DateTime.UtcNow.AddDays(-50),
                SensorModel = sensorModel2,
                Location = brno,
                ImagePath = "images/sensors/s3.jpg"
            };

            var sensorB4T = new Sensor
            {
                Id = 4,
                Name = "BSensor4",
                DisplayName = "Temperature-A1",
                InstallationDate = DateTime.UtcNow.AddDays(-50),
                SensorModel = sensorModel4Temp,
                Location = brno,
                ImagePath = "images/sensors/s4.jpg"
            };

            var sensorB5W = new Sensor
            {
                Id = 5,
                Name = "BSensor5",
                DisplayName = "Water-A1",
                InstallationDate = DateTime.UtcNow.AddDays(-50),
                SensorModel = sensorModel5Water,
                Location = brno,
                ImagePath = "images/sensors/s5.jpg"
            };

            var sensorB6WT = new Sensor
            {
                Id = 6,
                Name = "BSensor6",
                DisplayName = "Water Temperature-A1",
                InstallationDate = DateTime.UtcNow.AddDays(-50),
                SensorModel = sensorModel4Temp,
                Location = brno,
                ImagePath = "images/sensors/s6.jpg"
            };

            var sensors = new Sensor[] { sensorB1E, sensorB2Oil, sensorB3E, sensorB4T, sensorB5W, sensorB6WT };

            if (!await _context.Sensors.AnyAsync())
            {
                await _context.Sensors.AddRangeAsync(sensors);
            }
            #endregion SensorStuff

            #region Data

            if (!await _context.SensorData.AnyAsync())
            {
                var current = DateTime.UtcNow;
                const int freq = 5;

                const int dataCount = 5000;
                var data = new List<SensorData>();
                var arr = new SensorData[dataCount];

                var targetTime = current.AddHours(-48);
                var oil = 250d;
                var lastOilChange = current;

                var water = 2000d;

                TimeZoneInfo centralEuTimeZone; 
                if (OperatingSystem.IsWindows()) {
                    centralEuTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Central Europe Standard Time");
                } else {
                    centralEuTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Europe/Prague");
                }

                while (current > targetTime)
                {
                    current = current.AddMinutes(-freq);
                    var czechDatetime = TimeZoneInfo.ConvertTimeFromUtc(current, centralEuTimeZone);
                    bool isWorkingHour = czechDatetime.Hour >= 8 && czechDatetime.Hour <= 17;

                    var max = GetRandomBetweenPercentage(5000, 2);
                    var min = GetRandomBetweenPercentage(200, 2);
                    double value = isWorkingHour ? max : min;
                    data.Add(new SensorData { Data = value, Sensor = sensorB1E, Time = current });

                    value = isWorkingHour ? GetRandomBetweenPercentage(2000, 10) : GetRandomBetweenPercentage(150, 3);
                    data.Add(new SensorData { Data = value, Sensor = sensorB3E, Time = current });

                    value = isWorkingHour ? GetRandomBetweenPercentage(23f, 5f) : GetRandomBetweenPercentage(18f, 5f);
                    data.Add(new SensorData { Data = Math.Round(value, 1), Sensor = sensorB4T, Time = current });

                    if (isWorkingHour && lastOilChange >= current)
                    {
                        oil += GetRandomBetweenPercentage(10d, 50d);
                        if (oil >= 990)
                        {
                            oil = 150;
                        }
                        lastOilChange = current.AddMinutes(-GetRandomBetweenPercentage(30, 50));
                    }

                    data.Add(new SensorData { Data = Math.Round(oil, 1), Sensor = sensorB2Oil, Time = current });

                    water += GetRandomBetweenPercentage(0.2, 10);
                    if (water >= 1900)
                    {
                        water = 1500;
                    }
                    data.Add(new SensorData { Data = water, Sensor = sensorB5W, Time = current });

                    value = isWorkingHour ? GetRandomBetweenPercentage(60f, 5f) : GetRandomBetweenPercentage(25, 1f);
                    data.Add(new SensorData { Data = Math.Round(value, 1), Sensor = sensorB6WT, Time = current });
                }

                await _context.SensorData.AddRangeAsync(data);
            }
            #endregion Data

            #region Dashboards

            if (!await _context.Dashboards.AnyAsync())
            {
                var gaugeDef = new CircularGaugeDefinitionDto
                {
                    Maximum = 100,
                    Minimum = 0,
                    Ranges = new GaugeRangeDto[]{
                        new GaugeRangeDto{Start = 0, End = 30, Color = "#30B32D"},
                        new GaugeRangeDto{Start = 30, End = 70, Color = "#FFDD00"},
                        new GaugeRangeDto{Start = 70, End = 100, Color = "#F03E3E"}
                    }
                };
                var defJson = JsonConvert.SerializeObject(gaugeDef);

                var dg1 = new Panel { Id = 1, Name = "Group 1", Sensor = sensorB1E, SizeX = 2, SizeY = 1, Type = "chart" };
                var dg2 = new Panel { Id = 2, Name = "Group 2", Sensor = sensorB1E, SizeX = 1, SizeY = 1, Type = "circularGauge", TypeDefinition = defJson };
                var dg3 = new Panel { Id = 3, Name = "Group 3", Sensor = sensorB1E };
                var dg4 = new Panel { Id = 4, Name = "Group 4", Sensor = sensorB1E };

                var panels = new List<Panel> { dg1, dg2 };//, dg2, dg3, dg4 };

                var dash = new Dashboard
                {
                    Id = 1,
                    Name = "Dashboard1",
                    DisplayName = "Dashboard 1",
                    Columns = 2,
                    IsGenerated = true,
                    Panels = panels,
                    Location = brno
                };


                var dash2 = new Dashboard
                {
                    Id = 2,
                    Name = "dashboard2",
                    DisplayName = "Example dashboard",
                    IsGenerated = false,
                    Url = "/dashboard/2",
                    Location = brno
                };

                var dashboards = new Dashboard[] { dash, dash2 };
                await _context.Dashboards.AddRangeAsync(dashboards);
            }

            #endregion Dashboards
            await _context.SaveChangesAsync();


            if (!await _context.Thresholds.AnyAsync())
            {
                var admin = await _context.Users
                    .AsNoTracking()
                    .FirstOrDefaultAsync(u => u.UserName == "admin");

                var userSessionProperty = _context.GetType().GetProperty("_userSession", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
                var userSession = (Shared.DataInterfaces.IUserSession)userSessionProperty.GetValue(_context);
                userSession.UserId = admin.Id;

                var threshold = new Threshold
                {
                    Id = 1,
                    Notify = "test.email@address.com",
                    MaxValue = 1000,
                    MinValue = 50,
                    Sensor = sensorB2Oil,
                    CreatedBy = admin.Id,
                    CreatedOn = DateTime.UtcNow.AddDays(-10)
                };

                var thresholds = new Threshold[] { threshold };

                await _context.Thresholds.AddRangeAsync(thresholds);
            }

            await _context.SaveChangesAsync();
        }

        private async Task SeedASPIdentityCoreAsync()
        {
            //await _context.Database.ExecuteSqlRawAsync($"TRUNCATE TABLE public.\"Users\" CASCADE;");
            //await _context.Database.ExecuteSqlRawAsync($"TRUNCATE TABLE public.\"Roles\" CASCADE;");
            //await _context.Database.ExecuteSqlRawAsync($"TRUNCATE TABLE public.\"RefreshToken\" CASCADE;");

            if (!await _context.Users.AnyAsync())
            {
                //Generating inbuilt accounts
                const string adminRoleName = "Administrator";
                const string userRoleName = "User";
                const string managerRoleName = "Manager";

                var managerPermissions = new string[] {
                    Permissions.Threshold.Read, Permissions.Threshold.Create, Permissions.Threshold.Update, Permissions.Threshold.Delete,
                    Permissions.Location.Read, Permissions.Location.Update };
                var adminPermissions = new string[] {
                    Permissions.Location.Create, Permissions.Location.Delete, Permissions.Location.Read
                };
                await EnsureRoleAsync(adminRoleName, "Administrator", adminPermissions);
                await EnsureRoleAsync(userRoleName, "User role", Array.Empty<string>());
                await EnsureRoleAsync(managerRoleName, "Manager role", managerPermissions);

                await CreateUserAsync("admin", "admin", "Admin", "Blazor", "Administrator", "admin@monitoring.com", "+420 777 777 777", new string[] { adminRoleName });
                await CreateUserAsync("pMaly", "pMaly", "Peter", "Maly", "Peter Maly", "jan.maly@email.cz", "+420 123 456 999", new string[] { userRoleName });
                await CreateUserAsync("jStredny", "jStredny", "Jan", "Stredny", "Jan Stredny", "jan.stredny@email.cz", "+420 123 456 777", new string[] { userRoleName });
                await CreateUserAsync("jVelky", "jVelky", "Jozef", "Velky", "Jozef Velky", "jan.velky@email.cz", "+420 123 456 888", new string[] { managerRoleName });

                _logger.LogInformation("Inbuilt account generation completed");
            }
        }

        private async Task SeedUserProfileAsync()
        {
            var usernames = new string[] { "pMaly", "jStredny", "jVelky" };
            foreach (var username in usernames)
            {
                ApplicationUser user = await _userManager.FindByNameAsync(username);

                if (!_context.UserProfiles.Any())
                {
                    UserProfile userProfile = new UserProfile
                    {
                        UserId = user.Id,
                        ApplicationUser = user,
                    };
                    _context.UserProfiles.Add(userProfile);
                }
            }

            await _context.SaveChangesAsync();
        }

        private async Task EnsureRoleAsync(string roleName, string description, string[] claims)
        {
            if ((await _roleManager.FindByNameAsync(roleName)) == null)
            {
                if (claims == null)
                {
                    claims = Array.Empty<string>();
                }

                string[] invalidClaims = claims.Where(c => ApplicationPermissions.GetPermissionByValue(c) == null).ToArray();
                if (invalidClaims.Any())
                {
                    throw new Exception("The following claim types are invalid: " + string.Join(", ", invalidClaims));
                }

                var applicationRole = new IdentityRole<Guid>(roleName);

                var result = await _roleManager.CreateAsync(applicationRole);

                IdentityRole<Guid> role = await _roleManager.FindByNameAsync(applicationRole.Name);

                foreach (string claim in claims.Distinct())
                {
                    result = await _roleManager.AddClaimAsync(role, new Claim(ClaimConstants.Permission, ApplicationPermissions.GetPermissionByValue(claim)));

                    if (!result.Succeeded)
                    {
                        await _roleManager.DeleteAsync(role);
                    }
                }
            }
        }

        private async Task<ApplicationUser> CreateUserAsync(string userName, string password, string firstName, string lastName, string fullName, string email, string phoneNumber, string[] roles)
        {
            var applicationUser = _userManager.FindByNameAsync(userName).Result;

            if (applicationUser == null)
            {
                applicationUser = new ApplicationUser
                {
                    UserName = userName,
                    Email = email,
                    PhoneNumber = phoneNumber,
                    FirstName = firstName,
                    LastName = lastName,
                    EmailConfirmed = true
                };

                var result = _userManager.CreateAsync(applicationUser, password).Result;
                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }

                result = _userManager.AddClaimsAsync(applicationUser, new Claim[]{
                        new Claim(ClaimTypes.Name, userName),
                        new Claim(ClaimTypes.Email, email),
                 }).Result;

                //add claims version of roles
                foreach (var role in roles.Distinct())
                {
                    await _userManager.AddClaimAsync(applicationUser, new Claim($"Is{role}", "true"));
                }

                ApplicationUser user = await _userManager.FindByNameAsync(applicationUser.UserName);

                try
                {
                    result = await _userManager.AddToRolesAsync(user, roles.Distinct());
                }
                catch
                {
                    await _userManager.DeleteAsync(user);
                    throw;
                }

                if (!result.Succeeded)
                {
                    await _userManager.DeleteAsync(user);
                }
            }
            return applicationUser;
        }

        private Random _rnd = new Random();

        private int GetRandomBetweenPercentage(int number, int spread)
        {
            double part = _rnd.NextDouble() * (2 * spread) / 100f;
            return (int)(number * (1 + part));
        }

        private double GetRandomBetweenPercentage(double number, double spread, int decimals = 2)
        {
            double part = _rnd.NextDouble() * (2 * spread) / 100f;
            return Math.Round(number * (1 + part), decimals);
        }
    }
}
