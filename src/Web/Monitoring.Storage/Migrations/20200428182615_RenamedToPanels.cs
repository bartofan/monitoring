﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Monitoring.Storage.Migrations
{
    public partial class RenamedToPanels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Panel_Dashboards_DashboardId",
                table: "Panel");

            migrationBuilder.DropForeignKey(
                name: "FK_Panel_Sensors_SensorId",
                table: "Panel");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Panel",
                table: "Panel");

            migrationBuilder.RenameTable(
                name: "Panel",
                newName: "Panels");

            migrationBuilder.RenameIndex(
                name: "IX_Panel_SensorId",
                table: "Panels",
                newName: "IX_Panels_SensorId");

            migrationBuilder.RenameIndex(
                name: "IX_Panel_DashboardId",
                table: "Panels",
                newName: "IX_Panels_DashboardId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Panels",
                table: "Panels",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Panels_Dashboards_DashboardId",
                table: "Panels",
                column: "DashboardId",
                principalTable: "Dashboards",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Panels_Sensors_SensorId",
                table: "Panels",
                column: "SensorId",
                principalTable: "Sensors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Panels_Dashboards_DashboardId",
                table: "Panels");

            migrationBuilder.DropForeignKey(
                name: "FK_Panels_Sensors_SensorId",
                table: "Panels");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Panels",
                table: "Panels");

            migrationBuilder.RenameTable(
                name: "Panels",
                newName: "Panel");

            migrationBuilder.RenameIndex(
                name: "IX_Panels_SensorId",
                table: "Panel",
                newName: "IX_Panel_SensorId");

            migrationBuilder.RenameIndex(
                name: "IX_Panels_DashboardId",
                table: "Panel",
                newName: "IX_Panel_DashboardId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Panel",
                table: "Panel",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Panel_Dashboards_DashboardId",
                table: "Panel",
                column: "DashboardId",
                principalTable: "Dashboards",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Panel_Sensors_SensorId",
                table: "Panel",
                column: "SensorId",
                principalTable: "Sensors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
