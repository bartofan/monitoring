﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Monitoring.Storage.Migrations
{
    public partial class Change13 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ImagePath",
                table: "SensorTypes");

            migrationBuilder.DropColumn(
                name: "Manufacturer",
                table: "SensorTypes");

            migrationBuilder.DropColumn(
                name: "Model",
                table: "SensorTypes");

            migrationBuilder.DropColumn(
                name: "Note",
                table: "SensorTypes");

            migrationBuilder.DropColumn(
                name: "PowerSupplyVoltage",
                table: "SensorTypes");

            migrationBuilder.AddColumn<int>(
                name: "UnitId",
                table: "SensorTypes",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SensorModelId",
                table: "Sensors",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Units",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(nullable: true),
                    Symbol = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Units", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SensorModels",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Manufacturer = table.Column<string>(nullable: true),
                    Model = table.Column<string>(nullable: true),
                    PowerSupplyVoltage = table.Column<string>(nullable: true),
                    ImagePath = table.Column<string>(nullable: true),
                    Note = table.Column<string>(nullable: true),
                    UnitId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SensorModels", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SensorModels_Units_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Units",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SensorTypes_UnitId",
                table: "SensorTypes",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_Sensors_SensorModelId",
                table: "Sensors",
                column: "SensorModelId");

            migrationBuilder.CreateIndex(
                name: "IX_SensorModels_UnitId",
                table: "SensorModels",
                column: "UnitId");

            migrationBuilder.AddForeignKey(
                name: "FK_Sensors_SensorModels_SensorModelId",
                table: "Sensors",
                column: "SensorModelId",
                principalTable: "SensorModels",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SensorTypes_Units_UnitId",
                table: "SensorTypes",
                column: "UnitId",
                principalTable: "Units",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Sensors_SensorModels_SensorModelId",
                table: "Sensors");

            migrationBuilder.DropForeignKey(
                name: "FK_SensorTypes_Units_UnitId",
                table: "SensorTypes");

            migrationBuilder.DropTable(
                name: "SensorModels");

            migrationBuilder.DropTable(
                name: "Units");

            migrationBuilder.DropIndex(
                name: "IX_SensorTypes_UnitId",
                table: "SensorTypes");

            migrationBuilder.DropIndex(
                name: "IX_Sensors_SensorModelId",
                table: "Sensors");

            migrationBuilder.DropColumn(
                name: "UnitId",
                table: "SensorTypes");

            migrationBuilder.DropColumn(
                name: "SensorModelId",
                table: "Sensors");

            migrationBuilder.AddColumn<string>(
                name: "ImagePath",
                table: "SensorTypes",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Manufacturer",
                table: "SensorTypes",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Model",
                table: "SensorTypes",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Note",
                table: "SensorTypes",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PowerSupplyVoltage",
                table: "SensorTypes",
                type: "text",
                nullable: true);
        }
    }
}
