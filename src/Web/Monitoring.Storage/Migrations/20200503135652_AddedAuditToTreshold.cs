﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Monitoring.Storage.Migrations
{
    public partial class AddedAuditToTreshold : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Thresholds_AspNetUsers_CreatedById",
                table: "Thresholds");

            migrationBuilder.DropIndex(
                name: "IX_Thresholds_CreatedById",
                table: "Thresholds");

            migrationBuilder.DropColumn(
                name: "CreatedById",
                table: "Thresholds");

            migrationBuilder.AddColumn<Guid>(
                name: "CreatedBy",
                table: "Thresholds",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOn",
                table: "Thresholds",
                nullable: false,
                defaultValueSql: "CURRENT_TIMESTAMP");

            migrationBuilder.AddColumn<Guid>(
                name: "ModifiedBy",
                table: "Thresholds",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOn",
                table: "Thresholds",
                nullable: false,
                defaultValueSql: "CURRENT_TIMESTAMP");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Thresholds");

            migrationBuilder.DropColumn(
                name: "CreatedOn",
                table: "Thresholds");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Thresholds");

            migrationBuilder.DropColumn(
                name: "ModifiedOn",
                table: "Thresholds");

            migrationBuilder.AddColumn<Guid>(
                name: "CreatedById",
                table: "Thresholds",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Thresholds_CreatedById",
                table: "Thresholds",
                column: "CreatedById");

            migrationBuilder.AddForeignKey(
                name: "FK_Thresholds_AspNetUsers_CreatedById",
                table: "Thresholds",
                column: "CreatedById",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
