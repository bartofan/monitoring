﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Monitoring.Storage.Migrations
{
    public partial class Change15 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SensorTypeId",
                table: "SensorModels",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SensorModels_SensorTypeId",
                table: "SensorModels",
                column: "SensorTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_SensorModels_SensorTypes_SensorTypeId",
                table: "SensorModels",
                column: "SensorTypeId",
                principalTable: "SensorTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SensorModels_SensorTypes_SensorTypeId",
                table: "SensorModels");

            migrationBuilder.DropIndex(
                name: "IX_SensorModels_SensorTypeId",
                table: "SensorModels");

            migrationBuilder.DropColumn(
                name: "SensorTypeId",
                table: "SensorModels");
        }
    }
}
