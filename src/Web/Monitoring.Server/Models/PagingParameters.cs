namespace Monitoring.Server.Models
{
    public class PagingParameters
    {
        private int _pageSize = 10;
        private const int maxPageSize = 20;

        public int PageNumber { get; set; } = 1;

        public int PageSize
        {
            get => _pageSize;
            set
            {
                _pageSize = System.Math.Min(value, maxPageSize);
            }
        }

        public string OrderBy { get; set; } = null;
    }
}
