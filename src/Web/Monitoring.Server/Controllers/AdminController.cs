﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Monitoring.Server.Data.Core;
using Monitoring.Server.Middleware.Wrappers;
using Monitoring.Shared.DataModels;
using Monitoring.Shared.AuthorizationDefinitions;
using Monitoring.Shared.Dto;
using Monitoring.Shared.Dto.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using static Microsoft.AspNetCore.Http.StatusCodes;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace Monitoring.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Policy = Policies.IsAdmin)]
    public class AdminController : ControllerBase
    {
        private readonly ILogger<AccountController> _logger;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole<Guid>> _roleManager;

        public AdminController(
            UserManager<ApplicationUser> userManager,
            ILogger<AccountController> logger,
            RoleManager<IdentityRole<Guid>> roleManager
            )
        {
            _userManager = userManager;
            _logger = logger;
            _roleManager = roleManager;
        }

        [HttpGet("Users")]
        public async Task<object> GetUsers()
        {
            List<ApplicationUser> applicationUsers;
            var userDtoList = new List<UserInfoDto>();

            IQueryCollection query = Request.Query;
            var count = _userManager.Users.Count();
            try
            {
                if (query.Keys.Contains("$inlinecount"))
                {
                    int skip = (query.TryGetValue("$skip", out var Skip)) ? Convert.ToInt32(Skip[0]) : 0;
                    int top = (query.TryGetValue("$top", out var Take)) ? Convert.ToInt32(Take[0]) : count;

                    applicationUsers = await _userManager.Users.OrderBy(x => x.Id).Skip(skip).Take(top).ToListAsync();
                }
                else
                {
                    applicationUsers = await _userManager.Users.ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(null, ex);
            }

            try
            {
                foreach (var applicationUser in applicationUsers)
                {
                    userDtoList.Add(new UserInfoDto
                    {
                        FirstName = applicationUser.FirstName,
                        LastName = applicationUser.LastName,
                        UserName = applicationUser.UserName,
                        PhoneNumber = applicationUser.PhoneNumber,
                        Email = applicationUser.Email,
                        UserId = applicationUser.Id,
                        Roles = (List<string>)await _userManager.GetRolesAsync(applicationUser).ConfigureAwait(true)
                    });
                }
            }
            catch (Exception ex)
            {
                throw new Exception(null, ex);
            }

            if (query.Keys.Contains("$inlinecount"))
            {
                return new
                {
                    Items = userDtoList,
                    Count = count
                };
            }
            else
            {
                return new ApiResponse(Status200OK, "User list fetched", userDtoList);
            }
        }

        [HttpGet("Permissions")]
        public ApiResponse GetPermissions()
        {
            var permissions = ApplicationPermissions.GetAllPermissionNames();
            return new ApiResponse(Status200OK, "Permissions list fetched", permissions);
        }

        [HttpGet("Roles")]
        public async Task<object> GetRoles()
        {
            List<IdentityRole<Guid>> roles;
            var roleDtoList = new List<RoleDto>();

            IQueryCollection query = Request.Query;
            var count = _roleManager.Roles.Count();
            try
            {
                if (query.Keys.Contains("$inlinecount"))
                {
                    int skip = query.TryGetValue("$skip", out var Skip) ? Convert.ToInt32(Skip[0]) : 0;
                    int top = query.TryGetValue("$top", out var Take) ? Convert.ToInt32(Take[0]) : count;

                    roles = await _roleManager.Roles.OrderBy(x => x.Id).Skip(skip).Take(top).ToListAsync();
                }
                else
                {
                    roles = await _roleManager.Roles.ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(null, ex);
            }

            try
            {
                foreach (var role in roles)
                {
                    var claims = await _roleManager.GetClaimsAsync(role);
                    var permissions = claims
                        .Where(x => x.Type == "permission")
                        .Select(x => ApplicationPermissions.GetPermissionByValue(x.Value).Name)
                        .ToList();

                    roleDtoList.Add(new RoleDto
                    {
                        Name = role.Name,
                        Permissions = permissions
                    });
                }
            }
            catch (Exception ex)
            {
                throw new Exception(null, ex);
            }

            if (query.Keys.Contains("$inlinecount"))
            {
                return new
                {
                    Items = roleDtoList,
                    Count = count
                };
            }
            else
            {
                return new ApiResponse(Status200OK, "Roles list fetched", roleDtoList);
            }
        }

        [HttpGet("Role/{roleName}")]
        [Authorize]
        public async Task<ApiResponse> GetRoleAsync(string roleName)
        {
            RoleDto roleDto;
            IdentityRole<Guid> identityRole;

            // get paginated list of users
            try
            {
                identityRole = await _roleManager.FindByNameAsync(roleName);
            }
            catch (Exception ex)
            {
                throw new Exception(null, ex);
            }

            try
            {
                var claims = await _roleManager.GetClaimsAsync(identityRole);
                //var permissions = claims.Where(x => x.Type == "permission").Select(x => ApplicationPermissions.GetPermissionByValue(x.Value).Name).ToList();

                roleDto = new RoleDto
                {
                    Name = roleName,
                    //Permissions = permissions
                };
            }
            catch (Exception ex)
            {
                throw new Exception(null, ex);
            }

            return new ApiResponse(Status200OK, "Role fetched", roleDto);
        }

        [HttpPost("Role")]
        //[Authorize(Permissions.Role.Create)]
        public async Task<ApiResponse> CreateRoleAsync([FromBody] RoleDto newRole)
        {
            try
            {
                // first make sure the role doesn't already exist
                if (_roleManager.Roles.Any(r => r.Name == newRole.Name))
                {
                    return new ApiResponse(Status400BadRequest, "Role already exists");
                }

                // Create the role
                var result = await _roleManager.CreateAsync(new IdentityRole<Guid>(newRole.Name));

                if (!result.Succeeded)
                {
                    string errorMessage = result.Errors.Select(x => x.Description).Aggregate((i, j) => i + " - " + j);
                    return new ApiResponse(Status500InternalServerError, errorMessage);
                }

                // Re-create the permissions
                IdentityRole<Guid> role = await _roleManager.FindByNameAsync(newRole.Name);

                foreach (string claim in newRole.Permissions)
                {
                    var resultAddClaim = await _roleManager.AddClaimAsync(role, new Claim(ClaimConstants.Permission, ApplicationPermissions.GetPermissionByName(claim)));

                    if (!resultAddClaim.Succeeded)
                    {
                        await _roleManager.DeleteAsync(role);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Role failed to create", newRole.Name, newRole.FormattedPermissions);
                return new ApiResponse(Status500InternalServerError, ex.Message);
            }

            return new ApiResponse(Status200OK);
        }

        [HttpPut("Role")]
        //[Authorize(Permissions.Role.Update)]
        public async Task<ApiResponse> UpdateRoleAsync([FromBody] RoleDto newRole)
        {
            try
            {
                // first make sure the role already exist
                if (!_roleManager.Roles.Any(r => r.Name == newRole.Name))
                {
                    return new ApiResponse(Status400BadRequest, "This role doesn't exists");
                }

                // Create the permissions
                IdentityRole<Guid> identityRole = await _roleManager.FindByNameAsync(newRole.Name);

                var claims = await _roleManager.GetClaimsAsync(identityRole);
                var permissions = claims
                    .Where(x => x.Type == ClaimConstants.Permission)
                    .Select(x => x.Value)
                    .ToList();

                foreach (var permission in permissions)
                {
                    await _roleManager.RemoveClaimAsync(identityRole, new Claim(ClaimConstants.Permission, permission));
                }

                foreach (string claim in newRole.Permissions)
                {
                    var result = await _roleManager.AddClaimAsync(identityRole, new Claim(ClaimConstants.Permission, ApplicationPermissions.GetPermissionByName(claim)));

                    if (!result.Succeeded)
                    {
                        await _roleManager.DeleteAsync(identityRole);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Role failed to update", newRole.Name, newRole.FormattedPermissions);
                return new ApiResponse(Status500InternalServerError, ex.Message);
            }
            return new ApiResponse(Status200OK);
        }

        // DELETE: api/Admin/Role/5
        [HttpDelete("Role/{name}")]
        //[Authorize(Permissions.Role.Delete)]
        public async Task<ApiResponse> DeleteRoleAsync(string name)
        {
            try
            {
                // Check if the role is used by a user
                var users = await _userManager.GetUsersInRoleAsync(name);
                if (users.Any())
                {
                    return new ApiResponse(Status404NotFound, "This role is still used by a user, you cannot delete it");
                }

                // Delete the role
                var role = await _roleManager.FindByNameAsync(name);
                await _roleManager.DeleteAsync(role);

                return new ApiResponse(Status200OK, "Role Deletion Successful");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Role failed to delete", name);
                return new ApiResponse(Status400BadRequest, "Role Deletion Failed");
            }
        }
    }
}
