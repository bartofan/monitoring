﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Monitoring.Storage;
using Monitoring.Server.Middleware.Wrappers;
using Monitoring.Shared.Dto;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Microsoft.AspNetCore.Http.StatusCodes;
using System;
using Syncfusion.Blazor.Data;
using Monitoring.Shared.DataModels;

namespace Monitoring.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DataController : ControllerBase
    {
        private readonly ApplicationDbContext _db;
        private readonly IMapper _mapper;

        public DataController(ApplicationDbContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ApiResponse> Get([FromQuery] int sensorId, DateTime from, DateTime to)
        {
            var days = from == default(DateTime) || to == default(DateTime)
                ? 14
                : (int)(to - from).TotalDays + 1;
            var time = GetInterval(days);
            var interval = $"'{days} days'";
            // Console.WriteLine(interval);
            // Console.WriteLine(time);

            var result = await _db.SensorData
                .FromSqlRaw($@"
                    select
	                    time_bucket('{time}', ""Time"" ) as Time,
	                    avg(""Data"") as Data
                    from
	                    ""SensorData""
                    where
	                    ""Time"" > now() - interval {interval}
                        and ""SensorId"" = {sensorId}
                    group by
	                    Time
                    order by
	                    Time")
                .Select(x => new
                {
                    Time = x.Time,
                    Data = x.Data
                })
                .AsNoTracking()
                .ToListAsync();

            return new ApiResponse(Status200OK, "Retrieved sensor types", result);
        }

        private string GetInterval(int diff)
        {
            switch (diff)
            {
                case var d when (d <= 2):
                    return "30 minutes";
                case var d when (d <= 4):
                    return "2 hours";
                case var d when (d <= 7):
                    return "4 hours";
                case var d when (d <= 14):
                    return "12 hours";
                case var d when (d <= 28):
                    return "1 day";
                case var d when (d <= 60):
                    return "2 days";
                case var d when (d <= 120):
                    return "4 days";
                default:
                    return "1 day";
            }
        }

        [HttpGet("raw")]
        public async Task<ApiResponse> Get1([FromQuery] int sensorId, DateTime from, DateTime to)
        {
            var result = await _db.SensorData
                .Where(sd => sd.SensorId == sensorId
                    && sd.Time > from
                    && sd.Time < to)
                .OrderBy(s => s.Time)
                .ToListAsync();

            return new ApiResponse(Status200OK, "Retrieved data", _mapper.Map<List<SensorDataDto>>(result));
        }

        [HttpGet("last")]
        public async Task<ApiResponse> Get2([FromQuery] int sensorId)
        {
            var result = await _db.SensorData
                .Where(sd => sd.SensorId == sensorId)
                .OrderByDescending(sd => sd.Time)
                .FirstAsync();

            return new ApiResponse(Status200OK, "Retrieved data", _mapper.Map<SensorDataDto>(result));
        }
    }
}
