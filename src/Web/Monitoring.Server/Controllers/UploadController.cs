using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Monitoring.Shared.AuthorizationDefinitions;
using Monitoring.Storage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Monitoring.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Policy = Policies.IsAdmin)]
    public class UploadController : ControllerBase
    {
        private readonly ApplicationDbContext _db;
        private readonly IWebHostEnvironment _hostingEnvinronment;

        public UploadController(ApplicationDbContext db, IWebHostEnvironment env)
        {
            _db = db;
            _hostingEnvinronment = env;
        }

        [HttpPost]
        public async Task Save(IList<IFormFile> UploadFiles)
        {
            var filename = Response.HttpContext.Request.Headers["FileName"].ToString();
            if (string.IsNullOrEmpty(filename)) {
                Response.StatusCode = 400;
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "File does not have a name.";
                return;
            }
            try
            {
                foreach (var file in UploadFiles)
                {
                    filename = filename.Trim('"');

                    var subfolder = Response.HttpContext.Request.Headers["SubFolder"].ToString();

                    filename = Path.Combine(
                        _hostingEnvinronment.WebRootPath,
                        "images",
                        string.IsNullOrWhiteSpace(subfolder) ? "" : subfolder,
                        filename);
                    using (var fs = new FileStream(filename, FileMode.Create))
                    {
                        await file.CopyToAsync(fs);
                        fs.Flush();
                    }
                }
            }
            catch (Exception e)
            {
                Response.Clear();
                Response.StatusCode = 400;
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "File failed to upload";
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = e.Message;
            }
        }
    }
}
