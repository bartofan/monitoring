﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Monitoring.Storage;
using Monitoring.Server.Middleware.Wrappers;
using Monitoring.Shared.DataModels;
using Monitoring.Shared.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Microsoft.AspNetCore.Http.StatusCodes;
using Microsoft.AspNetCore.Http;
using System.Linq;
using System;
using Monitoring.Shared.AuthorizationDefinitions;
using Monitoring.Server.Services.Contracts;

namespace Monitoring.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class SensorController : ControllerBase
    {
        private readonly ApplicationDbContext _db;
        private readonly IMapper _mapper;
        private readonly ISensorService _sensorService;

        public SensorController(ApplicationDbContext db, IMapper mapper, ISensorService sensorService)
        {
            _db = db;
            _mapper = mapper;
            _sensorService = sensorService;
        }

        [HttpGet]
        public async Task<object> Get()
        {
            var queryable = _db.Sensors
                .Include(s => s.Location)
                .Include(s => s.SensorModel);

            IQueryCollection query = Request.Query;
            if (query.Keys.Contains("$inlinecount"))
            {
                var count = queryable.Count();
                int skip = (query.TryGetValue("$skip", out var Skip)) ? Convert.ToInt32(Skip[0]) : 0;
                int top = (query.TryGetValue("$top", out var Take)) ? Convert.ToInt32(Take[0]) : count;

                var res = queryable.Skip(skip)
                                    .Take(top);
                var items = await _mapper.ProjectTo<SensorDto>(res).ToArrayAsync();
                return new
                {
                    Items = items,
                    Count = count
                };
            }

            var sensors = await queryable
                .ToListAsync();

            return new ApiResponse(Status200OK, "Retrieved sensors", _mapper.Map<List<SensorDto>>(sensors));
        }

        [HttpGet("{id}")]
        public async Task<ApiResponse> Get(int id)
        {
            Sensor sensors = await _db.Sensors
                .Include(s => s.Location)
                .Include(s => s.SensorModel).ThenInclude(m => m.SensorType)
                .Include(s => s.SensorModel).ThenInclude(m => m.Unit)
                .FirstOrDefaultAsync(s => s.Id == id);

            return new ApiResponse(Status200OK, "Retrieved sensor", _mapper.Map<SensorDto>(sensors));
        }

        [HttpPut]
        [Authorize(Policy = Policies.IsAdmin)]
        public async Task<ApiResponse> Put(SensorDto parameters)
        {
            if (!ModelState.IsValid)
            {
                return new ApiResponse(Status400BadRequest, "Sensor model is invalid.");
            }

            parameters.Location = null;
            parameters.SensorModel = null;

            return await _sensorService.Update(parameters);
        }

        [HttpPost("{id}/AddThreshold")]
        [Authorize(Permissions.Threshold.Create)]
        public async Task<ApiResponse> AddThreshold(int id, [FromBody] ThresholdCreateDto parameters)
        {
            if (!ModelState.IsValid)
            {
                return new ApiResponse(Status400BadRequest, "Threshold model is invalid.");
            }

            var sensor = await _db.Sensors.FirstOrDefaultAsync(s => s.Id == id);
            if (sensor == null)
            {
                return new ApiResponse(Status400BadRequest, "Not existing sensor");
            }

            try
            {
                var threshold = _mapper.Map<Threshold>(parameters);
                sensor.Thresholds.Add(threshold);
                await _db.SaveChangesAsync();
                return new ApiResponse(Status200OK);
            }
            catch (System.Exception e)
            {
                return new ApiResponse(Status500InternalServerError, "Error", null, new ApiError(e.Message));
            }
        }
    }
}
