﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Monitoring.Server.Middleware.Wrappers;
using Monitoring.Server.Models;
using Monitoring.Server.Services.Contracts;
using Monitoring.Shared.AuthorizationDefinitions;
using Monitoring.Shared.Dto;
using System;
using System.Threading.Tasks;
using static Microsoft.AspNetCore.Http.StatusCodes;

namespace Monitoring.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class LocationController : ControllerBase
    {
        private readonly ILogger<AccountController> _logger;
        private readonly ILocationService _locationService;

        public LocationController(ILocationService locationService, ILogger<AccountController> logger)
        {
            _logger = logger;
            _locationService = locationService;
        }

        [HttpGet]
        public async Task<object> Get()
        {
            IQueryCollection queryString = Request.Query;
            if (queryString.Keys.Contains("$inlinecount"))
            {
                return await _locationService.GetOData(queryString);
            }
            else
            {
                return await _locationService.GetAllAsync();
            }
        }

        [HttpGet("{id}")]
        public async Task<ApiResponse> Get(int id)
        {
            return await _locationService.GetAsync(id);
        }

        [HttpPost]
        [Authorize(Permissions.Location.Create)]
        public async Task<ApiResponse> Post(LocationDto locationDto)
        {
            if (!ModelState.IsValid)
            {
                return new ApiResponse(Status400BadRequest, "Location model is invalid.");
            }
            return await _locationService.Create(locationDto);
        }

        [HttpPut]
        [Authorize(Permissions.Location.Update)]
        public async Task<ApiResponse> Update(LocationDto locationDto)
        {
            if (!ModelState.IsValid)
            {
                return new ApiResponse(Status400BadRequest, "Location model is invalid.");
            }
            return await _locationService.Update(locationDto);
        }

        [HttpDelete]
        [Authorize(Permissions.Location.Delete)]
        public async Task<ApiResponse> Delete(int id)
        {
            return await _locationService.Delete(id);
        }
    }
}
