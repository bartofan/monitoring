﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Monitoring.Server.Middleware.Wrappers;
using Monitoring.Server.Services.Contracts;
using Monitoring.Shared.AuthorizationDefinitions;
using Monitoring.Shared.Dto.Account;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using static Microsoft.AspNetCore.Http.StatusCodes;

namespace Monitoring.Server.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        [HttpPost("ConfirmEmail")]
        [AllowAnonymous]
        public async Task<ApiResponse> ConfirmEmail(ConfirmEmailDto parameters)
        {
            if (parameters.UserId == null || parameters.Token == null)
            {
                return new ApiResponse(Status404NotFound, "User does not exist");
            }

            return await _accountService.ConfirmEmail(parameters);
        }

        [AllowAnonymous]
        [HttpPost("Login")]
        public async Task<ApiResponse> Login(LoginDto parameters)
        {
            if (!ModelState.IsValid)
            {
                return new ApiResponse(Status400BadRequest, "User model is invalid.");
            }

            ApiResponse loginResponse = await _accountService.Login(parameters);
            if (loginResponse.StatusCode == Status200OK)
            {
                var loginResult = (LoginResultDto)loginResponse.Result;
                SetTokenCookie(loginResult.RefreshToken, Response);
            }
            return loginResponse;
        }

        [AllowAnonymous]
        [HttpPost("RefreshToken")]
        public async Task<ApiResponse> Refresh()
        {
            var refreshToken = Request.Cookies["refresh-token"];
            Console.WriteLine(refreshToken);

            ApiResponse refreshTokenResponse = await _accountService.RefreshToken(refreshToken);
            if (refreshTokenResponse.StatusCode == Status200OK)
            {
                var refreshTokenResult = (LoginResultDto)refreshTokenResponse.Result;
                Console.WriteLine(refreshTokenResult.RefreshToken.Token);
                SetTokenCookie(refreshTokenResult.RefreshToken, Response);
            }

            return refreshTokenResponse;
        }

        [HttpPost("RevokeToken")]
        public async Task<ApiResponse> RevokeToken([FromBody] RevokeTokenDto revokeTokenDto)
        {
            return await _accountService.RevokeToken(revokeTokenDto.Token);
        }

        [HttpPost("Logout")]
        public async Task<ApiResponse> Logout()
        {
            var response = await _accountService.Logout();
            if (response.StatusCode == Status200OK)
            {
                Response.Cookies.Delete("refresh-token");
            }
            return response;
        }

        [HttpPost("UserChangePassword")]
        public async Task<ApiResponse> UserChangePassword(ChangePasswordDto parameters)
        {
            return await _accountService.UserChangePassword(parameters);
        }

        [HttpGet("UserInfo")]
        public async Task<ApiResponse> UserInfo()
        {
            //System.Console.WriteLine(User.Identity?.Name);
            //System.Console.WriteLine(User.Claims);
            return await _accountService.UserInfo(User);
        }

        // PUT: api/Account
        [HttpPut]
        public async Task<ApiResponse> UpdateUser(UserInfoDto userInfo)
        {
            if (!ModelState.IsValid)
            {
                return new ApiResponse(Status400BadRequest, "User Model is Invalid");
            }

            return await _accountService.UpdateUser(User, userInfo);
        }

        [HttpPost("Create")]
        [Authorize(Policy = Policies.IsAdmin)]
        public async Task<ApiResponse> Create(CreateUserDto parameters)
        {
            if (!ModelState.IsValid)
            {
                return new ApiResponse(Status400BadRequest, "User Model is Invalid");
            }

            return await _accountService.Create(parameters);
        }

        [HttpDelete("{id}")]
        [Authorize(Policy = Policies.IsAdmin)]
        public async Task<ApiResponse> Delete(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return new ApiResponse(Status400BadRequest, "Id is invalid");
            }

            return await _accountService.Delete(id);
        }

        [HttpGet("ListRoles")]
        [Authorize(Policy = Policies.IsAdmin)]
        public async Task<ApiResponse> ListRoles()
        {
            return await _accountService.ListRoles();
        }

        [HttpPost("AdminUserPasswordReset/{id}")]
        [Authorize(Policy = Policies.IsAdmin)]
        public async Task<ApiResponse> AdminResetUserPasswordAsync(Guid id, [FromBody] AdminResetPasswordDto adminResetPasswordDto)
        {
            if (!ModelState.IsValid)
            {
                return new ApiResponse(Status400BadRequest, "Model is invalid");
            }

            return await _accountService.AdminResetUserPasswordAsync(id, adminResetPasswordDto);
        }

        private void SetTokenCookie(RefreshTokenDto token, HttpResponse response)
        {
            var cookieOptions = new CookieOptions
            {
                HttpOnly = true,
                Expires = token.Expiry
            };
            response.Cookies.Delete("refresh-token");
            response.Cookies.Append("refresh-token", token.Token, cookieOptions);
        }
    }
}
