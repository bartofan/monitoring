﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Monitoring.Server.Middleware.Wrappers;
using Monitoring.Server.Services.Contracts;
using Monitoring.Shared.AuthorizationDefinitions;
using Monitoring.Shared.Dto;
using Monitoring.Storage;
using System.Threading.Tasks;
using static Microsoft.AspNetCore.Http.StatusCodes;

namespace Monitoring.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ThresholdController : ControllerBase
    {
        private readonly ApplicationDbContext db;
        private readonly IThresholdService _thresholdService;

        public ThresholdController(ApplicationDbContext db, IThresholdService thresholdService)
        {
            this.db = db;
            _thresholdService = thresholdService;
        }

        [HttpGet]
        [Authorize(Permissions.Threshold.Read)]
        public async Task<object> Get()
        {
            IQueryCollection queryString = Request.Query;
            if (queryString.Keys.Contains("$inlinecount"))
            {
                return await _thresholdService.GetOData(queryString);
            }
            else
            {
                return await _thresholdService.GetAllAsync();
            }
        }

        [HttpPut]
        [Authorize(Permissions.Threshold.Update)]
        public async Task<ApiResponse> Put(ThresholdDto parameters)
        {
            if (!ModelState.IsValid)
            {
                return new ApiResponse(Status400BadRequest, "Threshold model is invalid.");
            }

            return await _thresholdService.Update(parameters);
        }

        [HttpDelete]
        [Authorize(Permissions.Threshold.Delete)]
        public async Task<ApiResponse> Delete(int id)
        {
            return await _thresholdService.Delete(id);
        }
    }
}
