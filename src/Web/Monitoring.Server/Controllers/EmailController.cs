﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Monitoring.Server.Middleware.Wrappers;
using Monitoring.Server.Services.Contracts;
using Monitoring.Shared.Dto;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using static Microsoft.AspNetCore.Http.StatusCodes;

namespace Monitoring.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class EmailController : ControllerBase
    {
        private readonly IEmailService _emailService;

        public EmailController(IEmailService emailService)
        {
            _emailService = emailService;
        }

        [HttpPost("support")]
        public async Task<ApiResponse> Send(EmailSupportDto parameters)
        {
            if (!ModelState.IsValid)
            {
                return new ApiResponse(Status400BadRequest, "Email has wrong parameters");
            }

            // TODO - upravit
            var userEmail = User.FindFirstValue(ClaimTypes.Email);
            var userName = User.FindFirstValue(ClaimTypes.Name);
            //email.FromAddresses.Add(new EmailAddressDto(userName, userEmail));

            var supportEmail = "stefan.svinciak@gmail.com";
            var email = new EmailMessageDto();
            email.ToAddresses.Add(new EmailAddressDto("Support", supportEmail));

            email.Subject = parameters.Subject;
            email.Body = parameters.Body;

            try
            {
                await _emailService.SendEmailAsync(email);
                return new ApiResponse(Status200OK, "Email Successfuly Sent");
            }
            catch (Exception ex)
            {
                return new ApiResponse(Status500InternalServerError, ex.Message);
            }
        }
    }
}
