﻿using Monitoring.Server.Middleware.Wrappers;
using Monitoring.Shared.Dto.Account;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using static Microsoft.AspNetCore.Http.StatusCodes;
using Monitoring.Storage;
using System;
using System.Security.Claims;

namespace Monitoring.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UserProfileController : ControllerBase
    {
        private readonly ApplicationDbContext _db;

        public UserProfileController(ApplicationDbContext db)
        {
            _db = db;
        }

        // GET: api/UserProfile
        [HttpGet]
        public async Task<ApiResponse> Get()
        {
            var userId = new Guid(User.FindFirstValue(ClaimTypes.NameIdentifier));
            var userProfile = await _db.UserProfiles.FindAsync(userId);

            return new ApiResponse(Status200OK, "Retrieved User Profile", userProfile);
        }

        // POST: api/UserProfile
        [HttpPost]
        public async Task<ApiResponse> Upsert(UserProfileDto userProfile)
        {
            await Task.Delay(0);
            return new ApiResponse(Status200OK, "Retrieved User Profile", null);
        }
    }
}
