﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Monitoring.Storage;
using Monitoring.Server.Middleware.Wrappers;
using Monitoring.Shared.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Microsoft.AspNetCore.Http.StatusCodes;

namespace Monitoring.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DashboardController : ControllerBase
    {
        private readonly ApplicationDbContext _db;
        private readonly IMapper _mapper;

        public DashboardController(ApplicationDbContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ApiResponse> Get()
        {
            var dash = await _db.Dashboards.ToListAsync();
            var res = _mapper.Map<List<DashboardDto>>(dash);

            return new ApiResponse(Status200OK, "Retrieved dashboard", res);
        }

        [HttpGet("GetByLocation/{locationId}")]
        public async Task<ApiResponse> GetByLocation(int locationId)
        {
            var dash = await _db.Dashboards
                .Where(d => d.Location.Id == locationId)
                .ToListAsync();
            var res = _mapper.Map<List<DashboardDto>>(dash);

            return new ApiResponse(Status200OK, "Retrieved dashboard", res);
        }

        [HttpGet("{id}")]
        public async Task<ApiResponse> Get(int id)
        {
            var data = await _db.Dashboards
                .Include(x => x.Panels)
                .ThenInclude(x => x.Sensor)
                .SingleOrDefaultAsync(x => x.Id == id);
            var res = _mapper.Map<DashboardDto>(data);

            return new ApiResponse(Status200OK, "Retrieved dashboard", res);
        }
    }
}
