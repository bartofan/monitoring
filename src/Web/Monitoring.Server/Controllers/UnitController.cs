﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Monitoring.Server.Middleware.Wrappers;
using Monitoring.Server.Services.Contracts;
using Monitoring.Shared.AuthorizationDefinitions;
using Monitoring.Shared.Dto;
using Monitoring.Storage;
using System.Threading.Tasks;
using static Microsoft.AspNetCore.Http.StatusCodes;

namespace Monitoring.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UnitController : ControllerBase
    {
        private readonly ApplicationDbContext db;
        private readonly IUnitService _unitService;

        public UnitController(ApplicationDbContext db, IUnitService unitService)
        {
            this.db = db;
            _unitService = unitService;
        }

        [HttpGet]
        public async Task<object> Get()
        {
            IQueryCollection queryString = Request.Query;
            if (queryString.Keys.Contains("$inlinecount"))
            {
                return await _unitService.GetOData(queryString);
            }
            else
            {
                return await _unitService.GetAllAsync();
            }
        }

        [HttpPost]
        public async Task<ApiResponse> Post(UnitDto parameters)
        {
            if (!ModelState.IsValid)
            {
                return new ApiResponse(Status400BadRequest, "Unit model is invalid.");
            }

            return await _unitService.Create(parameters);
        }

        [HttpPut]
        [Authorize(Policy = Policies.IsAdmin)]
        public async Task<ApiResponse> Put(UnitDto parameters)
        {
            if (!ModelState.IsValid)
            {
                return new ApiResponse(Status400BadRequest, "Unit type model is invalid.");
            }

            return await _unitService.Update(parameters);
        }

        [HttpDelete]
        public async Task<ApiResponse> Delete(int id)
        {
            return await _unitService.Delete(id);
        }
    }
}
