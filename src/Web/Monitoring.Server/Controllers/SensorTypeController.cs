﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Monitoring.Server.Middleware.Wrappers;
using Monitoring.Server.Models;
using Monitoring.Server.Services.Contracts;
using Monitoring.Shared.AuthorizationDefinitions;
using Monitoring.Shared.Dto;
using Monitoring.Storage;
using System;
using System.Linq;
using System.Threading.Tasks;
using static Microsoft.AspNetCore.Http.StatusCodes;

namespace Monitoring.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class SensorTypeController : ControllerBase
    {
        private readonly ApplicationDbContext db;
        private readonly ISensorTypeService _sensorTypeService;

        public SensorTypeController(ApplicationDbContext db, ISensorTypeService sensorTypeService)
        {
            this.db = db;
            _sensorTypeService = sensorTypeService;
        }

        [HttpGet("{id}")]
        public async Task<ApiResponse> Get(int id)
        {
            if (id <= 0)
            {
                return new ApiResponse(Status400BadRequest, "Sensor type model is invalid.");
            }
            return await _sensorTypeService.GetAsync(id);
        }

        [HttpGet]
        public async Task<object> Get()
        {
            IQueryCollection queryString = Request.Query;
            if (queryString.Keys.Contains("$inlinecount"))
            {
                return await _sensorTypeService.GetOData(queryString);
            }
            else
            {
                return await _sensorTypeService.GetAllAsync();
            }
        }

        [HttpPost]
        [Authorize(Policy = Policies.IsAdmin)]
        public async Task<ApiResponse> Post(SensorTypeDto parameters)
        {
            if (!ModelState.IsValid)
            {
                return new ApiResponse(Status400BadRequest, "Sensor type model is invalid.");
            }

            return await _sensorTypeService.Create(parameters);
        }

        [HttpPut]
        [Authorize(Policy = Policies.IsAdmin)]
        public async Task<ApiResponse> Put(SensorTypeDto parameters)
        {
            if (!ModelState.IsValid)
            {
                return new ApiResponse(Status400BadRequest, "Sensor type model is invalid.");
            }

            return await _sensorTypeService.Update(parameters);
        }

        [HttpDelete]
        [Authorize(Policy = Policies.IsAdmin)]
        public async Task<ApiResponse> Delete(int id)
        {
            return await _sensorTypeService.Delete(id);
        }
    }
}
