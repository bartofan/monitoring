using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Monitoring.Server.Helpers;
using Monitoring.Server.Services.Contracts;
using Monitoring.Shared.DataModels;
using Monitoring.Shared.Dto;
using Monitoring.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Monitoring.Server.Utils
{
    public class ThresholdCheckService : IHostedService, IDisposable
    {
        private bool _disposed = false;
        private Timer _timer;
        private readonly IServiceScopeFactory scopeFactory;
        private ApplicationDbContext _db;
        private readonly IEmailService _emailService;
        private readonly ILogger<ThresholdCheckService> _logger;
        private const double NotifyMinuteInterval = 30;
        private DateTime _lastRun = DateTime.Now;

        public ThresholdCheckService(
            IServiceScopeFactory scopeFactory,
            IEmailService emailService,
            ILogger<ThresholdCheckService> logger)
        {
            this.scopeFactory = scopeFactory;
            _emailService = emailService;
            this._logger = logger;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(DoWork, null, TimeSpan.FromMinutes(1), TimeSpan.FromMinutes(5));

            return Task.CompletedTask;
        }

        private async void DoWork(object state)
        {
            using var scope = scopeFactory.CreateScope();

            _db = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

            IEnumerable<Threshold> thresholds = await _db.Thresholds
                .Include(t => t.Sensor)
                .ToListAsync();
            var now = DateTime.Now;
            foreach (var threshold in thresholds)
            {
                if (threshold.Notified.HasValue && 
                    threshold.Notified.Value.AddMinutes(NotifyMinuteInterval) > now)
                {
                    continue;
                }

                try
                {
                    var sensor = threshold.Sensor;
                    var data = await _db.SensorData
                        .Where(sd => sd.Time > _lastRun && sd.SensorId == sensor.Id)
                        .ToArrayAsync();
                    var (HasReached, value) = HasThresholdReached(threshold, data);
                    if (HasReached)
                    {
                        var email = new EmailMessageDto();
                        email = EmailTemplates.BuildThresholdNotify(email, sensor.Name, value, threshold.MinValue, threshold.MaxValue);
                        email.ToAddresses = threshold.Notify
                            .Split(';')
                            .Select(e => new EmailAddressDto(e, e))
                            .ToList();  

                        await _emailService.SendEmailAsync(email);
                        threshold.Notified = now;
                        await _db.SaveChangesAsync();
                        _logger.LogInformation($"Threshold reached on sensorId: \"{sensor.Id}\"");
                    }
                }
                catch (System.Exception e)
                {
                    _logger.LogError(e, $"Failed threshold {threshold.Id} check.");
                }
            }
            _lastRun = now;
        }

        public static (bool HasReached, double value) HasThresholdReached(Threshold threshold, SensorData[] data)
        {
            foreach (var d in data)
            {
                if (d.Data > threshold.MaxValue || d.Data < threshold.MinValue)
                {
                    return (true, d.Data);
                }

            }
            return (false, 0);
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                }

                _disposed = true;
            }
        }

        ~ThresholdCheckService()
        {
            Dispose(false);
        }
    }
}
