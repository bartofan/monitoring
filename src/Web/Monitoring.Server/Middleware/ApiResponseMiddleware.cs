﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;
using Monitoring.Server.Middleware.Extensions;
using Monitoring.Server.Middleware.Wrappers;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using static Microsoft.AspNetCore.Http.StatusCodes;

namespace Monitoring.Server.Middleware
{
    public class ApiResponseMiddleware
    {
        private readonly RequestDelegate _next;
        private ILogger<ApiResponseMiddleware> _logger;
        private readonly Func<object, Task> _clearCacheHeadersDelegate;

        public ApiResponseMiddleware(RequestDelegate next)
        {
            _next = next;
            _clearCacheHeadersDelegate = ClearCacheHeaders;
        }

        public async Task Invoke(HttpContext httpContext, ILogger<ApiResponseMiddleware> logger)
        {
            _logger = logger;

            try
            {
                if (!BeginWithPathApiPrefix(httpContext))
                {
                    await _next(httpContext);
                }
                else
                {
                    var originalBodyStream = httpContext.Response.Body;

                    using (var responseBody = new MemoryStream())
                    {
                        httpContext.Response.Body = responseBody;

                        try
                        {
                            var response = httpContext.Response;
                            response.Body = responseBody;
                            await _next.Invoke(httpContext);

                            if (httpContext.Response.StatusCode == Status200OK)
                            {
                                var body = await FormatResponse(httpContext.Response);
                                await HandleSuccessRequestAsync(httpContext, body, httpContext.Response.StatusCode);
                            }
                            else
                            {
                                await HandleNotSuccessRequestAsync(httpContext, httpContext.Response.StatusCode);
                            }

                            httpContext.Response.ContentLength = responseBody.Length;
                        }
                        catch (Exception ex)
                        {
                            _logger.LogWarning("An Inner Middleware exception occurred: " + ex.Message);
                            await HandleExceptionAsync(httpContext, ex);
                        }
                        finally
                        {
                            responseBody.Seek(0, SeekOrigin.Begin);
                            await responseBody.CopyToAsync(originalBodyStream);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (httpContext.Response.HasStarted)
                {
                    _logger.LogWarning("A aiddleware exception occurred, but response has already started!");
                    throw;
                }

                await HandleExceptionAsync(httpContext, ex);
                throw;
            }
        }

        private async Task HandleExceptionAsync(HttpContext httpContext, System.Exception exception)
        {
            _logger.LogError("Api Exception:", exception);

            ApiError apiError = null;
            ApiResponse apiResponse = null;
            int code = 0;

            if (exception is ApiException)
            {
                var ex = exception as ApiException;
                apiError = new ApiError(ResponseMessageEnum.ValidationError.GetDescription(), ex.Errors);
                apiError.ValidationErrors = ex.Errors;
                apiError.ReferenceErrorCode = ex.ReferenceErrorCode;
                apiError.ReferenceDocumentLink = ex.ReferenceDocumentLink;
                code = ex.StatusCode;
                httpContext.Response.StatusCode = code;
            }
            else if (exception is UnauthorizedAccessException)
            {
                apiError = new ApiError("Unauthorized Access");
                code = Status401Unauthorized;
                httpContext.Response.StatusCode = code;
            }
            else
            {
#if !DEBUG
                var msg = "An unhandled error occurred.";
                string stack = null;
#else
                var msg = exception.GetBaseException().Message;
                string stack = exception.StackTrace;
#endif

                apiError = new ApiError(msg)
                {
                    Details = stack
                };
                code = Status500InternalServerError;
                httpContext.Response.StatusCode = code;
            }

            httpContext.Response.ContentType = "application/json";

            apiResponse = new ApiResponse(code, ResponseMessageEnum.Exception.GetDescription(), null, apiError);

            await httpContext.Response.WriteAsync(JsonConvert.SerializeObject(apiResponse));
        }

        private Task HandleNotSuccessRequestAsync(HttpContext httpContext, int code)
        {
            ApiError apiError;

            if (code == Status404NotFound)
            {
                apiError = new ApiError(ResponseMessageEnum.NotFound.GetDescription());
            }
            else if (code == Status204NoContent)
            {
                apiError = new ApiError(ResponseMessageEnum.NotContent.GetDescription());
            }
            else if (code == Status405MethodNotAllowed)
            {
                apiError = new ApiError(ResponseMessageEnum.MethodNotAllowed.GetDescription());
            }
            else if (code == Status401Unauthorized)
            {
                apiError = new ApiError(ResponseMessageEnum.UnAuthorized.GetDescription());
            }
            else
            {
                apiError = new ApiError(ResponseMessageEnum.Unknown.GetDescription());
            }

            var apiResponse = new ApiResponse(code, apiError);
            httpContext.Response.StatusCode = code;
            httpContext.Response.ContentType = "application/json";
            return httpContext.Response.WriteAsync(JsonConvert.SerializeObject(apiResponse));
        }

        private Task HandleSuccessRequestAsync(HttpContext httpContext, object body, int code)
        {
            string jsonString = string.Empty;
            var bodyText = !body.ToString().IsValidJson() ? ConvertToJSONString(body) : body.ToString();

            ApiResponse apiResponse = null;

            if (!body.ToString().IsValidJson())
            {
                return httpContext.Response.WriteAsync(JsonConvert.SerializeObject(apiResponse));
            }
            else
            {
                bodyText = body.ToString();
            }

            dynamic bodyContent = JsonConvert.DeserializeObject<dynamic>(bodyText);
            Type type = bodyContent?.GetType();

            // Check to see if body is already an ApiResponse Class type
            if (type.Equals(typeof(Newtonsoft.Json.Linq.JObject)))
            {
                apiResponse = JsonConvert.DeserializeObject<ApiResponse>(bodyText);
                if (apiResponse.StatusCode == 0)
                {
                    apiResponse.StatusCode = code;
                }

                if ((apiResponse.Result != null) || (!string.IsNullOrEmpty(apiResponse.Message)))
                {
                    jsonString = JsonConvert.SerializeObject(apiResponse);
                }
                else
                {
                    apiResponse = new ApiResponse(code, ResponseMessageEnum.Success.GetDescription(), bodyContent, null);
                    jsonString = JsonConvert.SerializeObject(apiResponse);
                }
            }
            else
            {
                apiResponse = new ApiResponse(code, ResponseMessageEnum.Success.GetDescription(), bodyContent, null);
                jsonString = JsonConvert.SerializeObject(apiResponse);
            }

            httpContext.Response.ContentType = "application/json";
            return httpContext.Response.WriteAsync(jsonString);
        }

        private async Task<string> FormatResponse(HttpResponse response)
        {
            response.Body.Seek(0, SeekOrigin.Begin);
            var plainBodyText = await new StreamReader(response.Body).ReadToEndAsync();
            response.Body.Seek(0, SeekOrigin.Begin);

            return plainBodyText;
        }

        private bool BeginWithPathApiPrefix(HttpContext context)
        {
            return context.Request.Path.StartsWithSegments("/api") && !context.Request.Query.ContainsKey("$inlinecount");
        }

        private Task ClearCacheHeaders(object state)
        {
            var response = (HttpResponse)state;

            response.Headers[HeaderNames.CacheControl] = "no-cache";
            response.Headers[HeaderNames.Pragma] = "no-cache";
            response.Headers[HeaderNames.Expires] = "-1";
            response.Headers.Remove(HeaderNames.ETag);

            return Task.CompletedTask;
        }

        private string ConvertToJSONString(object rawJSON)
        {
            return JsonConvert.SerializeObject(rawJSON, JSONSettings());
        }

        private JsonSerializerSettings JSONSettings()
        {
            return new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                Converters = new List<JsonConverter> { new StringEnumConverter() }
            };
        }
    }
}
