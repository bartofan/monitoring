﻿using AutoMapper;
using Monitoring.Storage;
using Monitoring.Shared.DataModels;
using Monitoring.Server.Services.Contracts;
using Monitoring.Shared.Dto;

namespace Monitoring.Server.Services.Implementations
{
    public class UnitService : CrudServiceBase<Unit, UnitDto>, IUnitService
    {
        public UnitService(ApplicationDbContext db, IMapper mapper) : base(db, mapper)
        {
        }
    }
}
