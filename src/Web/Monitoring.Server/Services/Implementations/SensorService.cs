﻿using AutoMapper;
using Monitoring.Storage;
using Monitoring.Shared.DataModels;
using Monitoring.Server.Services.Contracts;
using Monitoring.Shared.Dto;

namespace Monitoring.Server.Services.Implementations
{
    public class SensorService : CrudServiceBase<Sensor, SensorDto>, ISensorService
    {
        public SensorService(ApplicationDbContext db, IMapper mapper) : base(db, mapper)
        {
        }
    }
}
    