﻿using AutoMapper;
using Monitoring.Storage;
using Monitoring.Shared.DataModels;
using Monitoring.Server.Services.Contracts;
using Monitoring.Shared.Dto;

namespace Monitoring.Server.Services.Implementations
{
    public class SensorTypeService : CrudServiceBase<SensorType, SensorTypeDto>, ISensorTypeService
    {
        public SensorTypeService(ApplicationDbContext db, IMapper mapper) : base(db, mapper)
        {
        }
    }
}
