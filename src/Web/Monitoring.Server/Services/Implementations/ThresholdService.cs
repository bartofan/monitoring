﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Monitoring.Server.Services.Contracts;
using Monitoring.Shared.DataModels;
using Monitoring.Shared.Dto;
using Monitoring.Shared.Dto.Account;
using Monitoring.Storage;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Monitoring.Server.Services.Implementations
{
    public class ThresholdService : CrudServiceBase<Threshold, ThresholdDto>, IThresholdService
    {
        public ThresholdService(ApplicationDbContext db, IMapper mapper) : base(db, mapper)
        {
        }

        public override async Task<object> GetOData(IQueryCollection query)
        {
            var count = Db.Thresholds.Count();
            int skip = query.TryGetValue("$skip", out var Skip) ? Convert.ToInt32(Skip[0]) : 0;
            int top = query.TryGetValue("$top", out var Take) ? Convert.ToInt32(Take[0]) : count;

            var queryable = Db.Thresholds
                                .Skip(skip)
                                .Take(top);

            var items = await Mapper.ProjectTo<ThresholdDto>(queryable).ToArrayAsync();

            var ids = items.Select(i => i.CreatedBy)
                .Concat(items.Select(i => i.ModifiedBy));

            var users = await Db.Users.Where(u => ids.Contains(u.Id))
                .ToListAsync();

            foreach (var item in items)
            {
                item.UserCreatedBy = Mapper.Map<UserDto>(users.FirstOrDefault(u => u.Id == item.CreatedBy));
                item.UserModifiedBy = Mapper.Map<UserDto>(users.FirstOrDefault(u => u.Id == item.ModifiedBy));
            }

            return new
            {
                Items = items,
                Count = count
            };
        }
    }
}
