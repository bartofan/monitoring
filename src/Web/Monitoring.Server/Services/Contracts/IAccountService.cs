﻿using Microsoft.AspNetCore.Mvc;
using Monitoring.Server.Middleware.Wrappers;
using Monitoring.Shared.Dto.Account;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Monitoring.Server.Services.Contracts
{
    public interface IAccountService
    {
        Task<ApiResponse> ConfirmEmail(ConfirmEmailDto parameters);

        Task<ApiResponse> Login(LoginDto parameters);

        Task<ApiResponse> RefreshToken(string token);

        Task<ApiResponse> RevokeToken(string token);

        Task<ApiResponse> Logout();

        Task<ApiResponse> UserInfo(ClaimsPrincipal user);

        Task<ApiResponse> UpdateUser(ClaimsPrincipal user, UserInfoDto userInfo);

        Task<ApiResponse> Create(CreateUserDto parameters);

        Task<ApiResponse> Delete(string id);

        Task<ApiResponse> ListRoles();

        Task<ApiResponse> AdminResetUserPasswordAsync(Guid id, [FromBody] AdminResetPasswordDto adminResetPasswordDto);

        Task<ApiResponse> UserChangePassword(ChangePasswordDto parameters);
    }
}
