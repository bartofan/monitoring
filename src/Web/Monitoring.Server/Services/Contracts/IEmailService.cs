﻿using Monitoring.Shared.Dto;
using System.Threading.Tasks;

namespace Monitoring.Server.Services.Contracts
{
    public interface IEmailService
    {
        Task<bool> SendEmailAsync(EmailMessageDto emailMessage);
    }
}
