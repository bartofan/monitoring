﻿using Monitoring.Shared.Dto;

namespace Monitoring.Server.Services.Contracts
{
    public interface ISensorTypeService : ICrudService<SensorTypeDto>
    {
    }
}
