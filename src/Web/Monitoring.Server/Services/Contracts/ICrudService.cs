﻿using Microsoft.AspNetCore.Http;
using Monitoring.Server.Middleware.Wrappers;
using Monitoring.Shared.Dto;
using System.Threading.Tasks;

namespace Monitoring.Server.Services.Contracts
{
    public interface ICrudService<TDto> where TDto : BaseDto
    {
        Task<object> GetOData(IQueryCollection query);

        Task<ApiResponse> GetAllAsync();

        Task<ApiResponse> GetAsync(int id);

        Task<ApiResponse> Create(TDto entityDto);

        Task<ApiResponse> Update(TDto entityDto);

        Task<ApiResponse> Delete(int id);
    }
}
