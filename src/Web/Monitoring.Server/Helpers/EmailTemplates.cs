﻿using System;
using Microsoft.Extensions.FileProviders;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using Monitoring.Shared.Dto;

namespace Monitoring.Server.Helpers
{
    public static class EmailTemplates
    {
        private static IWebHostEnvironment _webHostEnvironment;
        private static string newUserEmailTemplate;
        private static string newUserConfirmationEmailTemplate;
        private static string passwordResetTemplate;
        private static string thresholdNotifyTemplate;
        private static string forgotPasswordTemplate;

        public static void Initialize(IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
        }

        public static EmailMessageDto BuildNewUserConfirmationEmail(this EmailMessageDto emailMessage, string recepientName, string userName, string callbackUrl, string userId, string token)
        {
            if (newUserConfirmationEmailTemplate == null)
            {
                newUserConfirmationEmailTemplate = ReadPhysicalFile("Helpers/Templates/NewUserConfirmationEmail.template");
            }

            emailMessage.Body = newUserConfirmationEmailTemplate
                //.Replace("{name}", recepientName) // Uncomment if you want to add name to the registration form
                .Replace("{userName}", userName)
                .Replace("{callbackUrl}", callbackUrl);

            emailMessage.Subject = string.Format("Welcome {0} to Monitoring", recepientName);

            return emailMessage;
        }

        public static EmailMessageDto BuildNewUserEmail(this EmailMessageDto emailMessage, string fullName, string userName, string emailAddress, string password)
        {
            if (newUserEmailTemplate == null)
                newUserEmailTemplate = ReadPhysicalFile("Helpers/Templates/NewUserEmail.template");

            emailMessage.Body = newUserEmailTemplate
                //.Replace("{fullName}", fullName) // Uncomment if you want to add name to the registration form has First / Last Name
                .Replace("{fullName}", userName) //Comment out if you want have First / Last Name in registration form.
                .Replace("{userName}", userName)
                .Replace("{email}", emailAddress)
                .Replace("{password}", password);

            emailMessage.Subject = string.Format("Welcome {0} to Monitoring", userName);

            return emailMessage;
        }

        public static EmailMessageDto BuildForgotPasswordEmail(this EmailMessageDto emailMessage, string name, string callbackUrl, string token)
        {
            if (forgotPasswordTemplate == null)
                forgotPasswordTemplate = ReadPhysicalFile("Helpers/Templates/ForgotPassword.template");

            emailMessage.Body = forgotPasswordTemplate
                .Replace("{name}", name)
                .Replace("{token}", token)
                .Replace("{callbackUrl}", callbackUrl);

            emailMessage.Subject = string.Format("Blazor Boilerplate Forgot your Passord? [{0}]", name);

            return emailMessage;
        }

        public static EmailMessageDto BuildPasswordResetEmail(this EmailMessageDto emailMessage, string userName)
        {
            if (passwordResetTemplate == null)
                passwordResetTemplate = ReadPhysicalFile("Helpers/Templates/PasswordReset.template");

            emailMessage.Body = passwordResetTemplate
                .Replace("{userName}", userName);

            emailMessage.Subject = string.Format("Blazor Boilerplate Password Reset for {0}", userName);

            return emailMessage;
        }

        public static EmailMessageDto BuildThresholdNotify(this EmailMessageDto emailMessage, string sensorName, double value, double min, double max)
        {
            if (thresholdNotifyTemplate == null) {
                thresholdNotifyTemplate = ReadPhysicalFile("Helpers/Templates/ThresholdNotify.template");
            }

            emailMessage.Body = thresholdNotifyTemplate
                .Replace("{sensorName}", sensorName)
                .Replace("{value}", value.ToString())
                .Replace("{min}", min.ToString())
                .Replace("{max}", max.ToString());

            emailMessage.Subject = $"{sensorName} - Threshold limit has been reached!";

            return emailMessage;
        }

        private static string ReadPhysicalFile(string path)
        {
            if (_webHostEnvironment == null)
            {
                throw new InvalidOperationException($"{nameof(EmailTemplates)} is not initialized");
            }

            IFileInfo fileInfo = _webHostEnvironment.ContentRootFileProvider.GetFileInfo(path);

            if (!fileInfo.Exists)
            {
                throw new FileNotFoundException($"Template file located at \"{path}\" was not found");
            }

            using (var fs = fileInfo.CreateReadStream())
            {
                using (var sr = new StreamReader(fs))
                {
                    return sr.ReadToEnd();
                }
            }
        }
    }
}
