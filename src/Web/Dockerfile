FROM mcr.microsoft.com/dotnet/core/aspnet:3.1.6-buster-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:3.1.302-buster AS build
WORKDIR /src
COPY ["Monitoring.Server/Monitoring.Server.csproj", "Monitoring.Server/"]
COPY ["Monitoring.Client/Monitoring.Client.csproj", "Monitoring.Client/"]
COPY ["Monitoring.Shared/Monitoring.Shared.csproj", "Monitoring.Shared/"]
COPY ["Monitoring.Storage/Monitoring.Storage.csproj", "Monitoring.Storage/"]
COPY ["*.sln", "."]
RUN dotnet restore "Monitoring.Server/Monitoring.Server.csproj"
COPY . .
WORKDIR "/src/Monitoring.Server"
RUN dotnet build "Monitoring.Server.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Monitoring.Server.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
#ENTRYPOINT ["dotnet", "Monitoring.Server.dll"]

# FROM nginx:alpine AS final
# WORKDIR /usr/share/nginx/html
# COPY --from=publish /app/publish/ .
# COPY nginx.conf /etc/nginx/nginx.conf

# wait
COPY wait-for-it.sh wait-for-it.sh 
RUN chmod +x wait-for-it.sh