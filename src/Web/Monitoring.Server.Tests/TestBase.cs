﻿using System;
using Monitoring.Shared.DataModels;

namespace Monitoring.Server.Tests
{
    public class TestBase
    {
        public SensorData[] GetRandomData(int count, float from, float to)
        {
            var rnd = new Random();
            var result = new SensorData[count];

            for (int i = 0; i < count; i++)
            {
                var number = rnd.NextDouble() * (to - from) + from;
                result[i] = new SensorData { Id = i, Data = number };
            }

            return result;
        }
    }
}
