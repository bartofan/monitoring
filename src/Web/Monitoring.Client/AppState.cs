﻿using System;

namespace Monitoring.Client
{
    public class AppState
    {
        public event Action OnChange;

        private void NotifyStateChanged() => OnChange?.Invoke();
    }
}
