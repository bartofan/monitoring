using Syncfusion.Blazor.Notifications;
using Syncfusion.Blazor.Spinner;

namespace Monitoring.Client.Services.Contracts
{
    public interface ISuperService
    {
        SfToast SfToast { get; set; }
        SfSpinner SfSpinner { get; set; }

        void ShowSuccess(string title, string content = null);
        void ShowError(string title, string content = null);
        void ShowWarning(string title, string content = null);
        void ShowSpinner();
        void HideSpinner();
    }
}
