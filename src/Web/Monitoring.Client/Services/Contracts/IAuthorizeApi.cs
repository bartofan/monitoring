using Monitoring.Shared.Dto.Account;
using Monitoring.Shared.Dto;
using System.Threading.Tasks;

namespace Monitoring.Client.Services.Implementations
{
    public interface IAuthorizeApi
    {
        Task<ApiResponseDto> Login(LoginDto loginDto);

        Task<ApiResponseDto> Create(CreateUserDto createUserDto);

        Task<ApiResponseDto> UpdateUser(UserInfoDto userInfoDto);

        Task<ApiResponseDto> Logout();

        Task<ApiResponseDto> RefreshToken();

        Task<UserInfoDto> GetUserInfo();

        Task<ApiResponseDto> ConfirmEmail(ConfirmEmailDto confirmEmailDto);

        Task<ApiResponseDto> GetFromJsonAsyncc(string uri);
        Task<ApiResponseDto> PostAsJsonAsyncc<T>(string uri, T body);
        Task<ApiResponseDto> PutAsJsonAsyncc<T>(string uri, T body);
    }
}
