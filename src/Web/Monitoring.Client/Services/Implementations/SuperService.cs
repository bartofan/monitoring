using Monitoring.Client.Services.Contracts;
using Syncfusion.Blazor.Notifications;
using Syncfusion.Blazor.Spinner;

namespace Monitoring.Client.Services.Implementations
{
    public class SuperService : ISuperService
    {
        private const string SpinnerTarget = "#spinner";

        public SfToast SfToast { get; set; }
        public SfSpinner SfSpinner { get; set; }

        public void HideSpinner()
        {
            SfSpinner.HideSpinner(SpinnerTarget);
        }

        public void ShowSpinner()
        {
            SfSpinner.ShowSpinner(SpinnerTarget);
        }

        public void ShowError(string title, string content = null)
        {
            SfToast.Show(new ToastModel
            {
                Title = title,
                Content = content,
                CssClass = "e-toast-danger",
                Icon = "oi oi-circle-x",
            });
        }

        public void ShowSuccess(string title, string content = null)
        {
            SfToast.Show(new ToastModel
            {
                Title = title,
                Content = content,
                CssClass = "e-toast-success",
                Icon = "oi oi-circle-check",
            });
        }

        public void ShowWarning(string title, string content = null)
        {
            SfToast.Show(new ToastModel
            {
                Title = title,
                Content = content,
                CssClass = "e-toast-warning",
                Icon = "oi oi-warning",
            });
        }
    }
}
