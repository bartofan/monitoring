using Monitoring.Client.Services.Contracts;
using Monitoring.Shared.Dto;
using Monitoring.Shared.Dto.Account;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace Monitoring.Client.Services.Implementations
{
    public class UserProfileApi : IUserProfileApi
    {
        private readonly HttpClient _httpClient;

        public UserProfileApi(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<ApiResponseDto> Get()
        {
            return await _httpClient.GetFromJsonAsync<ApiResponseDto>("api/UserProfile/Get");
        }

        public async Task<ApiResponseDto> Upsert(UserProfileDto userProfile)
        {
            var response = await _httpClient.PostAsJsonAsync("api/UserProfile/Upsert", userProfile);
            return await response.Content.ReadFromJsonAsync<ApiResponseDto>();
        }
    }
}
