﻿using Microsoft.JSInterop;
using System;
using System.Threading.Tasks;

namespace Monitoring.Client.Extensions
{
    public static class JSRuntimeExtensions
    {
        public static ValueTask<object> SaveAs(this IJSRuntime js, string filename, byte[] data)
            => js.InvokeAsync<object>("saveAsFile", filename, Convert.ToBase64String(data));

        public static ValueTask ClearActiveItems(this IJSRuntime js)
            => js.InvokeVoidAsync("clearActiveItems");

        public static ValueTask SetActiveItem(this IJSRuntime js)
            => js.InvokeVoidAsync("setActiveItem");
    }
}
