﻿using FluentValidation;
using Microsoft.Extensions.Localization;
using Monitoring.Shared.Dto;

namespace Monitoring.Client.Validators
{
    public class RoleValidator : AbstractValidator<RoleDto>
    {
        public RoleValidator(IStringLocalizer<App> localizer)
        {
            RuleFor(t => t.Name).NotEmpty().WithMessage(_ => localizer["Validation.NotEmpty"]);
        }
    }
}
