﻿using FluentValidation;
using Microsoft.Extensions.Localization;
using Monitoring.Shared.Dto;

namespace Monitoring.Client.Validators
{
    public class ThresholdValidator : AbstractValidator<ThresholdDto>
    {
        public ThresholdValidator(IStringLocalizer<App> localizer)
        {
            RuleFor(t => t.MinValue).NotNull().WithMessage(_ => localizer["Validation.Required"]);
            RuleFor(t => t.MinValue)
                .LessThanOrEqualTo(t => t.MaxValue ?? double.MaxValue)
                .WithMessage(_ => localizer["Validation.Threshold.MinLessMax"]);
            RuleFor(t => t.MaxValue).NotNull().WithMessage(_ => localizer["Validation.Required"]);
            RuleFor(t => t.Notify).NotEmpty().WithMessage(_ => localizer["Validation.NotEmpty"]);
        }
    }
}
