﻿using FluentValidation;
using Microsoft.Extensions.Localization;
using Monitoring.Shared.Dto.Account;

namespace Monitoring.Client.Validators
{
    public class ChangePasswordValidator : AbstractValidator<ChangePasswordDto>
    {
        public ChangePasswordValidator(IStringLocalizer<App> localizer)
        {
            RuleFor(t => t.Password).NotEmpty().WithMessage(_ => localizer["Validation.NotEmpty"]);
            RuleFor(t => t.PasswordConfirm).NotEmpty().WithMessage(_ => localizer["Validation.NotEmpty"]);
            RuleFor(t => t.OldPassword).NotEmpty().WithMessage(_ => localizer["Validation.NotEmpty"]);
        }
    }
}
