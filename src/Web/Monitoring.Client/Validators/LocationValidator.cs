﻿using FluentValidation;
using Microsoft.Extensions.Localization;
using Monitoring.Shared.Dto;

namespace Monitoring.Client.Validators
{
    public class LocationValidator : AbstractValidator<LocationDto>
    {
        public LocationValidator(IStringLocalizer<App> localizer)
        {
            RuleFor(t => t.Name).NotEmpty().WithMessage(_ => localizer["Validation.NotEmpty"]);
            RuleFor(t => t.DisplayName).NotEmpty().WithMessage(_ => localizer["Validation.NotEmpty"]);
            RuleFor(t => t.City).NotEmpty().WithMessage(_ => localizer["Validation.NotEmpty"]);
            RuleFor(t => t.Address).NotEmpty().WithMessage(_ => localizer["Validation.NotEmpty"]);
        }
    }
}
