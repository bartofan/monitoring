using System.Threading.Tasks;

namespace Monitoring.Shared
{
    public interface IDatabaseInitializer
    {
        Task SeedAsync();
    }
}