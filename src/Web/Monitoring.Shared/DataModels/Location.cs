﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Monitoring.Shared.DataInterfaces;

namespace Monitoring.Shared.DataModels
{
    public class Location : IEntity<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        [Phone]
        public string PhoneContact { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string ZipCode { get; set; }
        public string Note { get; set; }
        public virtual List<Sensor> Sensors { get; set; }
        public virtual List<Dashboard> Dashboards { get; set; }
    }
}
