﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Monitoring.Shared.DataModels
{
    public class SensorData
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public DateTime Time { get; set; }
        public double Data { get; set; }
        public int SensorId { get; set; }
        public virtual Sensor Sensor { get; set; }
    }
}
