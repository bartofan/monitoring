﻿using System;
using System.ComponentModel.DataAnnotations;
using Monitoring.Shared.DataInterfaces;

namespace Monitoring.Shared.DataModels
{
    public class Threshold : IEntity<int>, IAuditable, ISoftDelete
    {
        public int Id { get; set; }
        public double MinValue { get; set; }
        public double MaxValue { get; set; }
        [Required]
        public virtual Sensor Sensor { get; set; }
        public DateTime? Notified { get; set; }
        public string Notify { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
