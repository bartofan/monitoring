﻿using System;

namespace Monitoring.Shared.DataModels
{
    public class RefreshToken
    {
        public int Id { get; set; }
        public DateTime Expiry { get; set; }
        public bool IsExpired => DateTime.UtcNow >= Expiry;
        public DateTime Created { get; set; }
        public DateTime? Revoked { get; set; }
        public bool IsActive => Revoked == null && !IsExpired;
        public string Token { get; set; }
    }
}
