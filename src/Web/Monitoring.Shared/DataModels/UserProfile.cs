﻿using System;

namespace Monitoring.Shared.DataModels
{
    public class UserProfile
    {
        public Guid Id { get; set; }

        public Guid UserId { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }

        public string LastPageVisited { get; set; } = "/";
        public DateTime LastLoginDate { get; set; } = DateTime.MinValue;
    }
}
