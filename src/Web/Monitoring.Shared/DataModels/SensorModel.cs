﻿using System.Collections.Generic;
using Monitoring.Shared.DataInterfaces;

namespace Monitoring.Shared.DataModels
{
    public class SensorModel : IEntity<int>
    {
        public int Id { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string PowerSupplyVoltage { get; set; }
        public string ImagePath { get; set; }
        public string Note { get; set; }

        public virtual Unit Unit { get; set; }
        public virtual SensorType SensorType { get; set; }
        public virtual List<Sensor> Sensors { get; set; }
    }
}
