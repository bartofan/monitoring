﻿namespace Monitoring.Shared.DataModels
{
    public class Panel
    {
        public int Id { get; set; }
        public int Col { get; set; }
        public int SizeX { get; set; }
        public int SizeY { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string TypeDefinition { get; set; }
        public virtual Sensor Sensor { get; set; }
    }
}
