﻿using System.Collections.Generic;

namespace Monitoring.Shared.DataModels
{
    public class Dashboard
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public bool IsGenerated { get; set; }
        public string Url { get; set; }
        public int Columns { get; set; }
        public virtual List<Panel> Panels { get; set; }
        public virtual Location Location { get; set; }
    }
}
