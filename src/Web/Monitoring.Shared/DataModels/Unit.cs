using System.Collections.Generic;
using Monitoring.Shared.DataInterfaces;

namespace Monitoring.Shared.DataModels
{
    public class Unit : IEntity<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Symbol { get; set; }

        public virtual ICollection<SensorModel> SensorModel { get; set; }
    }
}
