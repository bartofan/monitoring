﻿namespace Monitoring.Shared.AuthorizationDefinitions
{
    public static class Actions
    {
        public const string Create = nameof(Create);
        public const string Read = nameof(Read);
        public const string Update = nameof(Update);
        public const string Delete = nameof(Delete);
    }

    public static class Permissions
    {
        public static class Threshold
        {
            public const string Create = nameof(Threshold) + "." + nameof(Actions.Create);
            public const string Read = nameof(Location) + "." + nameof(Actions.Read);
            public const string Update = nameof(Threshold) + "." + nameof(Actions.Update);
            public const string Delete = nameof(Threshold) + "." + nameof(Actions.Delete);
        }

        public static class Location
        {
            public const string Create = nameof(Location) + "." + nameof(Actions.Create);
            public const string Read = nameof(Location) + "." + nameof(Actions.Read);
            public const string Update = nameof(Location) + "." + nameof(Actions.Update);
            public const string Delete = nameof(Location) + "." + nameof(Actions.Delete);
        }
    }
}
