﻿namespace Monitoring.Shared.Dto
{
    public class SensorModelDto : BaseDto
    {
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string PowerSupplyVoltage { get; set; }
        public string ImagePath { get; set; }
        public string Note { get; set; }

        public UnitDto Unit { get; set; }
        public SensorTypeDto SensorType { get; set; }
    }
}
