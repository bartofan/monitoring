﻿using System.Collections.Generic;

namespace Monitoring.Shared.Dto
{
    public class RoleDto
    {
        public string Name { get; set; }

        public IEnumerable<string> Permissions { get; set; }

        public string FormattedPermissions => string.Join(", ", Permissions);
    }
}
