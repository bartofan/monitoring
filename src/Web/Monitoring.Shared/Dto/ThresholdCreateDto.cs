﻿using System.ComponentModel.DataAnnotations;

namespace Monitoring.Shared.Dto
{
    public class ThresholdCreateDto : BaseDto
    {
        [Required]
        public double? MinValue { get; set; }
        [Required]
        public double? MaxValue { get; set; }
        public SensorDto Sensor { get; set; }
        public int SensorId { get; set; }
        [Required]
        public string Notify { get; set; }
    }
}
