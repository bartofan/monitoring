﻿namespace Monitoring.Shared.Dto.Api
{
    public class PagedResultDto<TItems> //where TItems : class
    {
        public TItems[] Result { get; set; }
        public int Count { get; set; }
        public int PageSize { get; set; }
        public int TotalPages { get; set; }
    }
}
