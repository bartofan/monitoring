namespace Monitoring.Shared.Dto.Api
{
    public class ODataResultDto
    {
        public object[] Items { get; set; }
        public int Count { get; set; }
    }
}
