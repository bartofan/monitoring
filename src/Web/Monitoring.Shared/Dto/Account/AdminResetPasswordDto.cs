using System;
using System.ComponentModel.DataAnnotations;

namespace Monitoring.Shared.Dto.Account
{
    public class AdminResetPasswordDto
    {
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

    }
}
