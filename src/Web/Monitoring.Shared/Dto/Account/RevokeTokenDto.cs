﻿namespace Monitoring.Shared.Dto.Account
{
    public class RevokeTokenDto
    {
        public string Token { get; set; }
    }
}
