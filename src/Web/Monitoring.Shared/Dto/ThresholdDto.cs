﻿using Monitoring.Shared.Dto.Account;
using System;
using System.ComponentModel.DataAnnotations;

namespace Monitoring.Shared.Dto
{
    public class ThresholdDto : BaseDto
    {
        [Required]
        public double? MinValue { get; set; }
        [Required]
        public double? MaxValue { get; set; }
        public SensorDto Sensor { get; set; }
        public int SensorId { get; set; }
        public Guid CreatedBy { get; set; }
        public UserDto UserCreatedBy { get; set; }
        public Guid ModifiedBy { get; set; }
        public UserDto UserModifiedBy { get; set; }
        public DateTime? Notified { get; set; }
        [Required]
        public string Notify { get; set; }
    }
}
