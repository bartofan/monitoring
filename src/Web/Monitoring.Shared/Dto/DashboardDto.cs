﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Monitoring.Shared.Dto
{
    public class DashboardDto
    {
        [Required]
        public int Id { get; set; }

        public string Name { get; set; }
        public string DisplayName { get; set; }
        public int Columns { get; set; }
        public bool IsGenerated { get; set; }

        public virtual List<PanelDto> Panels { get; set; }
    }
}
