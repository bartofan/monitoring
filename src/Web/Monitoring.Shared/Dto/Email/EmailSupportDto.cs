﻿using System.ComponentModel.DataAnnotations;

namespace Monitoring.Shared.Dto
{
    public class EmailSupportDto
    {
        [Required]
        public string Subject { get; set; }

        [Required]
        public string Body { get; set; }
    }
}
