using System.Collections.Generic;

namespace Monitoring.Shared.Dto
{
    public class CircularGaugeDefinitionDto
    {
        public int Minimum { get; set; }
        public int Maximum { get; set; }
        public IEnumerable<GaugeRangeDto> Ranges { get; set; }
    }
}
