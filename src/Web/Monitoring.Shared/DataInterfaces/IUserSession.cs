using System;
using System.Collections.Generic;

namespace Monitoring.Shared.DataInterfaces
{
    public interface IUserSession
    {
        Guid UserId { get; set; }
        List<string> Roles { get; set; }
        string UserName { get; set; }
    }
}
