namespace Monitoring.Shared.DataInterfaces
{
    public interface IEntity<TKey>
    {
        public TKey Id { get; set; }
    }
}
