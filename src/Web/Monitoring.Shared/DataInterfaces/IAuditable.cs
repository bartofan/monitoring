﻿using System;

namespace Monitoring.Shared.DataInterfaces
{
    public interface IAuditable
    {
        Guid CreatedBy { get; set; }
        DateTime CreatedOn { get; set; }
        Guid ModifiedBy { get; set; }
        DateTime ModifiedOn { get; set; }
    }
}
