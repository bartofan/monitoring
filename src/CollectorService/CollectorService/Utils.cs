﻿using System;

namespace CollectorService
{
    public static class Utils
    {
        private static readonly Random _rnd = new Random();
        private static readonly object _syncLock = new object();

        public static int GetRandomBetweenPercentage(int number, int spread)
        {
            lock (_syncLock)
            {
                double part = _rnd.NextDouble() * (2 * spread) / 100f;
                return (int)(number * (1 + part));
            }
        }

        public static double GetRandomBetweenPercentage(double number, double spread, int decimals = 2)
        {
            lock (_syncLock)
            {
                double part = _rnd.NextDouble() * (2 * spread) / 100f;
                return Math.Round(number * (1 + part), decimals);
            }
        }
    }
}
